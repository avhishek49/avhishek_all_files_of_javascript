// Why do you use switch case in js?


//A switch statement can replace multiple if checks.

//It gives a more descriptive way to compare a value with multiple variants.


// syntax
switch(x) {
    case 'value1':  // if (x === 'value1')
      
      break;
  
    case 'value2':  // if (x === 'value2')
    
      break;
  
    default:
      
      break;
  }


  //Example 1

  let a = 2 + 2;

switch (a) {
  case 3:
    console.log( 'Too small' );
  case 4:
    console.log( 'Exactly!' );
  case 5:
    console.log( 'Too big' );
  default:
    console.log( "I don't know such values" );
}





//example of switch case using function 



function function_name(month)

{
    
    let result;

    switch (month) {
        case 'january':
            result = 1;
            break;
        case 'February':
            result = 2;

            break;
        case 'March':
            result =3;

            break;
            case 'April':
                result =4;
    
                break;
                case 'May':
                result =5;
    
                break;
                case 'June':
                    result =6;
        
                    break;
                    case 'July':
                        result =7;
            
                        break;

                        case 'August':
                        result =8;
            
                        break;
                        case 'September':
                            result =9;
                
                            break;
                            case 'October':
                                result =10;
                    
                                break;
                                case 'November':
                                    result =11;
                        
                                    break;

                                    case 'December':
                                        result =12;
                            
                                        break;

                                    
                                
                            
                        
                
                

        default:
            break;
    }
    return result;
}

var smith=function_name('November');
console.log(smith);