// function makeRequest(location)
// {
//     return new promise((ressolve,reject)=>
//     {
//         console.log(`Making request to ${location}`)
//         if(location==='Google')
//         {
//             ressolve('Google says h')
//         }
//         else
//         {
//             reject('we can only talk to google')
//         }
//     })
    
    

// }

// function processRequest(response)
// {
//     return new promise((ressolve,reject)=>
//     {
//         console.log('processing response')
//         resolve(`Extra information +${response}`)

//     })
// }
// makeRequest('Google').then(response=>
//     {
//         console.log('response recieved')
//         return processRequest(response)
//     }).then(ProcessedResponse=>
//         {
//             console.log(ProcessedResponse)
//         }).catch(err=>
//             {
//                 console.log(err)
//             })


((commonMappingHelper) => {
    const { getDistrictDdList, getProvisionDdList, getFeatureDetail, getBranchList, getAdminDetailsByIds, getUserIdbyAccount, getProfileDd } = require('../services/grpc_services');
    commonMappingHelper.arrayJoinList = (request, array2, primaryKey, foreignKey, selectFields) => {
      try {
        let array1 = [];
        let isArray = Array.isArray(request);
        array1 = !isArray ? [request] : request;
        for (let i = 0; i < array1.length; i++) {
          for (let j = 0; j < array2.length; j++) {
            if (array1[i][primaryKey] == array2[j][foreignKey]) {
              for (let k = 0; k < selectFields.length; k++) {
                if (array2[j].hasOwnProperty(selectFields[k].key)) {
                  array1[i][selectFields[k].as] = array2[j][selectFields[k].key] ? array2[j][selectFields[k].key] : array1[i][selectFields[k].as];
                }
              }
            }
          }
        }
        return !isArray ? array1[0] : array1;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.arrayJoinObject = (obj, array2, primaryKey, foreignKey, selectFields) => {
      // selectFields as [{key : 'provice' , as : 'districtName'}]
      try {
        for (let j = 0; j < array2.length; j++) {
          if (obj[primaryKey] == array2[j][foreignKey]) {
            for (let k = 0; k < selectFields.length; k++) {
              if (array2[j].hasOwnProperty(selectFields[k].key)) {
                obj[selectFields[k].as] = array2[j][selectFields[k].key];
              }
            }
          }
        }
        return obj;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.provinceMapper = async (metaHeader, request, primaryKey) => {
      try {
        primaryKey = primaryKey || 'district';
        const proviceList = await getProvisionDdList();
        const mappedObject = commonMappingHelper.arrayJoinObject(request, proviceList, 'province', 'id', [{ key: 'province', as: 'provinceName' }]);
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
    commonMappingHelper.districtMapper = async (metaHeader, request, primaryKey) => {
      try {
        primaryKey = primaryKey || 'district';
        const districtList = await getDistrictDdList(metaHeader);
        const mappedObject = commonMappingHelper.arrayJoinObject(request, districtList, 'district', 'id', [{ key: 'district', as: 'districtName' }]);
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
    commonMappingHelper.branchMapper = async (metaHeader, request, primaryKey, fieldName) => {
      try {
        primaryKey = primaryKey || 'branch_id';
        let ids = [];
        if (!Array.isArray(request)) {
          ids = [request[primaryKey]];
        } else {
          ids = request.map((obj) => obj[primaryKey]);
        }
        let branchList = await getBranchList(metaHeader, { filter: { ids: ids.join(',') } });
        let mappedObject = commonMappingHelper.arrayJoinList(request, branchList, primaryKey, 'branchCode', [
          { key: 'name', as: 'bankBranchName' },
          { key: 'name', as: fieldName }
        ]);
  
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.branchListMapper = async (metaHeader, request, primaryKey) => {
      try {
        primaryKey = primaryKey || 'branch_id';
        let ids = request.map((obj) => obj[primaryKey]);
        const branchList = await getBranchList(metaHeader, { filter: { ids: ids.join(',') } });
        const mappedObject = commonMappingHelper.arrayJoinList(request, branchList, primaryKey, 'branchCode', [
          { key: 'name', as: 'bankBranch' },
          { key: 'name', as: 'bankBranchName' },
          { key: 'name', as: primaryKey },
          { key: 'district', as: 'district' },
          { key: 'province', as: 'province' } ,
          { key: 'name', as: 'branchName'}
        ]);
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.featureMapper = async (request, metaHeader) => {
      try {
        const keywordDetails = await getFeatureDetail(metaHeader, { isProduct: true, isFinancial: true });
        const mappedObject = commonMappingHelper.arrayJoinList(request, keywordDetails, 'keyword', 'featureKeyword', [{ key: 'feature', as: 'KeywordDescription' }]);
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.provinceListMapper = async (metaHeader, request) => {
      try {
        const proviceList = await getProvisionDdList();
        const mappedObject = commonMappingHelper.arrayJoinList(request, proviceList, 'province', 'id', [{ key: 'province', as: 'province' }]);
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.districtListMapper = async (metaHeader, request) => {
      try {
        const districtList = await getDistrictDdList();
        const mappedObject = commonMappingHelper.arrayJoinList(request, districtList, 'district', 'id', [{ key: 'district', as: 'district' }]);
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.adminListMapperByName = async (metaHeader, accountNum) => {
      try {
        const accountDetail = await getUserIdbyAccount(metaHeader, accountNum);
        return accountDetail.userId;
      } catch (error) {
        throw error;
      }
    };
    commonMappingHelper.accountDetailMapperByAccountNum = async (metaHeader, request) => {
      try {
        const branchList = await getBranchList(metaHeader);
        const mappedObject = commonMappingHelper.arrayJoinList(request, branchList, 'branchName', 'name', [
          { key: 'name', as: 'bankBranchName' },
          { key: 'district', as: 'branchProvince' },
          { key: 'province', as: 'branchDistrict' }
        ]);
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
    commonMappingHelper.accountDetailMapperByAccountNum = async (metaHeader, accountNum) => {
      try {
        const accountDetail = await getUserIdbyAccount(metaHeader, accountNum);
        return accountDetail.userId ? accountDetail.userId : '0';
      } catch (error) {
        throw error;
      }
    };
    commonMappingHelper.accountDetailByAccountNum = async (metaHeader, accountNum) => {
      try {
        const accountDetail = await getUserIdbyAccount(metaHeader, accountNum);
        return accountDetail;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.filterBranchQuery = async (metaHeader, request) => {
      const sqrString = require('sqlstring');
      try {
        let branchFilter = {};
        let queryString;
        if (request.filter.branchId) {
          queryString = ` and branch_id = ${request.filter.branchId}`;
        } else {
          if (request.filter.provinceId || request.filter.branchId) {
            if (request.filter.provinceId) {
              branchFilter.provinceId = request.filter.provinceId;
            }
            if (request.filter.districtId) {
              branchFilter.districtId = request.filter.districtId;
            }
            const branchList = await getBranchList(metaHeader, branchFilter);
            let branchArray = branchList.map((obj) => obj.branchId);
            queryString = branchArray.length > 1 ? sqrString.format(` and branch_id in (?)`, branchArray) : '';
          }
        }
        return queryString;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.adminListMapper = async (metaHeader, dataList, fieldName) => {
      try {
        let ids = dataList.map((obj) => +obj[fieldName]);
        const adminList = await getAdminDetailsByIds(metaHeader, ids);
        const mappedObject = commonMappingHelper.arrayJoinList(dataList, adminList, fieldName, 'id', [
          { key: 'username', as: 'initiatedBy' },
          { key: 'username', as: fieldName }
        ]);
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.groupBy = (groupObject, key) => {
      return groupObject.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    };
  
    commonMappingHelper.rccdFeature = async (request, metaHeader) => {
      try {
        const keywordDetails = await getFeatureDetail(metaHeader, { isProduct: false, isFinancial: true });
        let mappedObject = commonMappingHelper.arrayJoinList(request, keywordDetails, 'featureKeyword', 'featureKeyword', [{ key: 'feature', as: 'featureName' }]);
        mappedObject = mappedObject.filter(function (obj) {
          return obj.hasOwnProperty('featureName');
        });
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.profileMapper = async (request, metaHeader) => {
      try {
        let ddRequest = {
          filter: {
            isCorporate: 0
          }
        };
        const profiledd = await getProfileDd(metaHeader, ddRequest);
        const mappedObject = commonMappingHelper.arrayJoinObject(request, profiledd, 'profileId', 'id', [{ key: 'profileName', as: 'profileName' }]);
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
  
    commonMappingHelper.profileListMapper = async (request, metaHeader) => {
      try {
        let ddRequest = {
          filter: {
            isCorporate: 0
          }
        };
        const profiledd = await getProfileDd(metaHeader, ddRequest);
        const mappedObject = commonMappingHelper.arrayJoinList(request, profiledd, 'profileId', 'id', [{ key: 'profileName', as: 'profileName' }]);
        return mappedObject;
      } catch (error) {
        throw error;
      }
    };
  })(module.exports);
  