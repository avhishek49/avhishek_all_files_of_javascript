//call back functions

// what is call back functions ?

// if the function will be called after sometime later in your code so this is called call back function :
function x()
{

}
x(function y()
{

})


//example

setTimeout(() => {
console.log("timer")    
}, 5000);

function x (y)
{
    console.log("x");
}

x(function y()
{
    console.log("Hello world");
})


// example 2

const  url1='https://stock.adobe.com/search?k=js ';
function download( url,callback)
{
    setTimeout(() => {
        console.log(`downloading ${url1}`)
        
    }, 3000);
}



/*




*/