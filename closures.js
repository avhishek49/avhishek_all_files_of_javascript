// closures in js

// combination of a function bundled together is closures


function x()
{
    var a=49;
    function y()
    {
        console.log(a);
    }

 y();
}
x();

// example 2


function makeFunc() {
    var name = 'Steve smith';
    function displayName() {
      console.log(name);
    }
    return displayName;
  }
  
  var myFunc = makeFunc();
  myFunc();


  
//Example 3

const Myname="smudge";
function printname()
{
    console.log(Myname);
}
printname();


// Myname ="joey"
//latest value will be printed of global scope

// main concept of closure is that the return value of one function is get stored so scope will not be allowed in this concept
// 

